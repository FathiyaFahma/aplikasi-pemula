<body>
    <nav class=navbar navbar-expand-md navbar-dark fixed-top bg-dark>
        <div class=container>
            <a class=navbar-brand href=#>CI Shop</a>
            <button class=navbar-toggler type=button data-toggle=collapse data-target=#navbarCollapse aria-controls=navbarCollapse aria-expanded=false aria-label=Toggle navigation>
                <span class=navbar-toggler-icon></span>
            </button>

            <div class=collapse navbar-collapse id=navbarCollapse>
                <ul class=navbar-nav mr-auto>
                    <li class=nav-item active>
                        <a class=nav-link href=index.html>Home <span class=sr-only>(current)</span></a>
                    </li>
                    <li class=nav-item dropdown>
                        <a href=# class=nav-link dropdown id=dropdown-1 data-toggle=dropdown aria-haspopup=true aria-expanded=false>Manage</a>
                        <div class=dropdown-menu aria-labelledby=dropdown-1>
                            <a href=/manage_category.html class=dropdown-item> Category </a>
                            <a href=/manage_product.html class=dropdown-item> Product </a>
                            <a href=/manage_order.html class=dropdown-item> Order </a>
                            <a href=/manage_user.html class=dropdown-item> Users </a>
                        </div>
                    </li>
                </ul>
                <ul class=navbar-nav>
                    <li class=nav-item>
                        <a href=/add_cart.html class=nav-link><i class=fa fa-shopping-cart></i> Cart (0)</a>
                    </li>
                    <li class=navbar-nav>
                        <a href=/login.html class=nav-link>Login</a>
                    </li>
                    <li class=navbar-nav>
                        <a href=/register.html class=nav-link>Register</a>
                    </li>
                    <li class=nav-item dropdown>
                        <a href=# class=nav-link dropdown id=dropdown-2 data-toggle=dropdown aria-haspopup=true aria-expanded=false>Administrator</a>
                        <div class=dropdown-menu aria-labelledby=dropdown-1>
                            <a href=/profile.html class=dropdown-item> Profile </a>
                            <a href=/orders.html class=dropdown-item> Orders </a>
                            <a href=/logout.html class=dropdown-item> Logout </a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</body>
